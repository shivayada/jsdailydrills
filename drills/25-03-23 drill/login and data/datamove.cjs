
// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const unlink = promisify(fs.unlink); 
const readFile = promisify(fs.readFile)
const startfile = 'lipsum.txt'
const finalfile = 'final.txt'




writeFile(startfile, 'Lorem sfsadfasf afsadf f asdf  asfgsr')
.then(()=>{
    console.log(`created ${startfile} file`)
    return readFile(startfile)
}).then((data)=>{
    console.log("read file")
    writeFile(finalfile , data)
})
.then(()=>{
    console.log("data written in final file")
    unlink(startfile)
})
.then(()=> console.log("start file deleted"))
.catch(err => console.log(err))