const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
const unlink = promisify(fs.unlink);



const file1= 'a.txt'
const file2 = 'b.txt'

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)



Promise.all([writeFile(file1,"hey i am a.txt "), writeFile(file2," hey i am b.txt")])
    .then(()=>{
        console.log('Files created successfully!');
    setTimeout(() => {

        unlink(file1)
        
        .then(()=>{
            console.log(`${file1} deleted`)
            unlink(file2)
            
        })
        .then(()=> console.log(`${file2} deleted`))
        .catch(err => {
            console.error(`Error deleting files: ${err}`);
          });
    },2000)
})
.catch(err=>{
    console.error(`Error creating files: ${err}`);
})
    
