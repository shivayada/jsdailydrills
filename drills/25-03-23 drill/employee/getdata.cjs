const fs = require('fs')


const data = require('./data.json')




function retriveids(info1, list, cb) {
    const arr = info1.employees

    const getid = arr.filter(ele => list.includes(ele.id))

    fs.writeFile('./retriveids.json', JSON.stringify(getid,null,2), (err) => {
        if (err) cb(err)
        cb(null, getid)
    })
    



}
function makegroups(info2, cb) {
    const makedata = info2.employees.reduce((acc, cur) => {
        if (cur.company in acc) {
            acc[cur.company].push(cur)
        } else {
            acc[cur.company] = []
            acc[cur.company].push(cur)
        }
        return acc

    }, {})
    fs.writeFile('./makegroups.json', JSON.stringify(makedata,null,2), (err) => {
        if (err) cb(err)
        cb(null, makedata)
    })

}
function removeId2(info3, cb) {
    info3 = info3.employees.filter(ele => ele.id != 2)


    const dataemp = {}
    dataemp["employees"] = info3
    fs.writeFile('./removeid2.json', JSON.stringify(dataemp,null,2), (err) => {
        if (err) cb(err)
        cb(null, dataemp)
    })
}


function getpowerpuff(info2, cb) {
    const getpp=[]

    if('Powerpuff Brigade' in info2){
        getpp.push(info2['Powerpuff Brigade'])
    }
    fs.writeFile('./powerpuff.json', JSON.stringify(getpp,null,2), (err) => {
        if (err) cb(err)
        cb(null, getpp)
    })
    
}

function sortOnCompany(info4, cb) {

    const sorteddata = info4.employees.sort((a, b) => {
        if (a.company === b.company) {
            return a.id - b.id;
        }
        return a.company.localeCompare(b.company);
    })
    const dataemp = {}
    dataemp["employees"] = sorteddata
    fs.writeFile('./sortoncompany.json', JSON.stringify(dataemp,null,2), (err) => {
        if (err) cb(err)
        cb(null, dataemp)
    })
}
function swap9392(info, cb) {
    const index93 = info.employees.findIndex(e => e.id === 93);
    const index94 = info.employees.findIndex(e => e.id === 94);

    const employee94 = info.employees.splice(index94, 1)[0];
    info.employees.splice(index93, 0, employee94);
    fs.writeFile('./swap9392.json', JSON.stringify(info,null,2), (err) => {
        if (err) cb(err)
        cb(null, info)
    })


}

function getbirthdaysEven(info, cb) {
    const today = new Date();
    const birthdate = `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`;

    info.employees.forEach((employee) => {
        if (employee.id % 2 === 0) {
            employee.birthday = birthdate;
        }
    });
    fs.writeFile('./getbirthday.json', JSON.stringify(info,null,2), (err) => {
        if (err) cb(err)
        cb(null, info)
    })
}




retriveids(data, [2, 13, 23], (err, info1) => {
    if (err) {
        console.log(err)
        return

    }
    makegroups(data, (err, info2) => {
        if (err) {
            console.log(err)
            return

        }
        getpowerpuff(info2, (err, info3) => {
            if (err) {
                console.log(err)
                return

            }
            removeId2(data, (err, info4) => {
                if (err) {
                    console.log(err)
                    return

                }
                sortOnCompany(info4, (err, info5) => {
                    if (err) {
                        console.log(err)
                        return

                    }
                    swap9392(info5, (err, info6) => {
                        if (err) {
                            console.log(err)
                            return

                        }
                        getbirthdaysEven(info6, (err, info7) => {
                            if (err) {
                                console.log(err)
                                return
    
                            }
                            console.log(info7)

                        })

                    })
                })
            })


        })

    })
})


