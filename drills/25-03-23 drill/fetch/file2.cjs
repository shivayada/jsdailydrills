

const usersurl = 'https://jsonplaceholder.typicode.com/users'
const todosurl = 'https://jsonplaceholder.typicode.com/todos'


// 2. Fetch all the todos
fetch(todosurl)
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch users')
        }
        return response.json()
    })
    .then(data => console.log(data[0]))
    .catch(error => console.error(error.message));