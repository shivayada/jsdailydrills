const usersurl = 'https://jsonplaceholder.typicode.com/users'
const todosurl = 'https://jsonplaceholder.typicode.com/todos'

// 1. Fetch all the users

fetch(usersurl)
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch users')
        }
        return response.json()
    })
    .then(data => console.log(data))
    .catch(error => console.error(error.message))