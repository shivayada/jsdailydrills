// 4. Use the promise chain and fetch the users first and then all the details for each user.

const usersurl = 'https://jsonplaceholder.typicode.com/users'
const todosurl = 'https://jsonplaceholder.typicode.com/todos'

fetch(usersurl)
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to fetch users')
        }
        return response.json()
    })
    .then(data => {
        const promises = data.map(user => {
            return fetch(`https://jsonplaceholder.typicode.com/users/${user.id}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Failed to fetch details for user ${user.id}`);
                    }
                    return response.json();
                })
                .then(details => {
                    console.log(`Details for user ${user.id}:`, details);
                    return details;
                });
        });
        return Promise.all(promises);
    })
    .catch(error => {
        console.error(error.message);
    });


