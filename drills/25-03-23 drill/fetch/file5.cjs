// 5. Use the promise chain and fetch the first todo.
//  Then find all the details for the user that is associated with that todo


const usersurl = 'https://jsonplaceholder.typicode.com/users'
const todosurl = 'https://jsonplaceholder.typicode.com/todos'

fetch(todosurl)
    .then(response =>{
        if(!response.ok){
            throw new Error('Failed to fetch todo')
        }
        return response.json()
    })
    .then(data =>{
        const userid = data[0].userId 
        if(userid===undefined){
            throw new Error('Failed to get userid')
        }
        fetch(usersurl)
            .then(response =>{
                if(!response.ok){
                    throw new Error('Failed to fetch users')
                }
                return response.json()

            })
            .then(userdata =>{
                const userdetails = userdata.filter(ele => ele.id===userid)
                if(userdetails === undefined){
                    throw new Error('Failed to get userdetails')
                }
                console.log(userdetails)
            })
            
    })
    .catch(error =>  console.error(error.message))


