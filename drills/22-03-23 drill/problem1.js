const fs = require('fs');
const path = require('path');
function create(dire,cb) {
   
    fs.mkdir(path.join(__dirname, dire), { recursive: true }, (err) => {
        if (err) {
            return console.error(err);
        }

    })
    console.log('Directory created successfully!');
    cb(dire)

}
function createfiles(num, dire, cb) {
    for (let i = 0; i < num; i++) {
        let filename =
            fs.writeFile(`./${dire}/output${i}.json`, '', 'utf8', function (err) {
                if (err) {
                    console.log("An error occured while writing JSON Object to File.");
                    return console.log(err);
                }
                
                console.log(`JSON file output${i}.json has been saved.`);
                cb(`./${dire}/output${i}.json`)
              
                

            });
          

    }
}
function deletefiles(filename) {
    fs.unlink(filename, function (err) {
        if (err) throw err;
        // if no error, file has been deleted successfully
        // console.log(`deleted ${filename}`);
       
    });
    console.log(`deleted ${filename}`);


}

function createDelete(num,dire) {
    create(dire,(dire) => {
        createfiles(num, dire, (filename) => {
            deletefiles(filename)
        })

    })
}



createDelete(parseInt(process.argv[2]), process.argv[3])


// module.exports = createDelete