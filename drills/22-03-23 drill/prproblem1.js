const fs = require('fs');
const path = require('path');

const dirPath = path.join(__dirname, 'my-json-files');
const numFiles = 10;

// Promisified version of fs.writeFile
const writeFile = (filePath, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

// Promisified version of fs.unlink
const unlink = (filePath) => {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

// Create the directory if it doesn't exist
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

// Generate random JSON data and write it to files
const filePromises = [];
for (let i = 1; i <= numFiles; i++) {
  const filePath = path.join(dirPath, `file${i}.json`);
  const jsonData = JSON.stringify({ data: Math.random() });
  filePromises.push(writeFile(filePath, jsonData));
}

// Delete the files simultaneously
Promise.all(filePromises.map((p) => p.then((filePath) => unlink(filePath))))
// chain unlink promises to filePromises 
  .then(() => {
    console.log(`Deleted ${numFiles} files successfully!`);
  })
  .catch((err) => {
    console.error(`Error deleting files: ${err}`);
  });
