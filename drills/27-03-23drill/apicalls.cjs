// Another small drill on promises:


// You are working on a web application that fetches data from an external API.
//  However, due to the API's rate limit restrictions, you are only allowed to make a maximum of 3 requests per second. 
// Your task is to write a function that makes API requests and returns a Promise that resolves with the response data.

// The function should accept the following arguments:

// url (string): The URL to the API endpoint.
// headers (object, optional): An object containing any headers to be sent with the request.
// params (object, optional): An object containing any query parameters to be sent with the request.


// The function should meet the following requirements:

// - It should return a Promise that resolves with the response data.
// - It should respect the API's rate limit restrictions by ensuring that no more than 3 requests are made per 3 second.
// - It should handle any errors that may occur during the request.
// // - You may use any external libraries or modules as needed.

let lastcallTime = 0
function callApi(url, headers = {}, params = {}) {

    const promise = new Promise((resolve, reject)=>{

        function request() {
            setTimeout(() => {
                console.log(new Date().toLocaleTimeString())
                fetch(url)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error("response not found")

                        }
                        return response.json()
                    })
                    .then(data => {
                       
                        return resolve(data)})
                    .catch(error => reject(error))


            }, lastcallTime)
        }
        request()
    })
    setTimeout(()=>{
    Promise.all([promise, promise, promise])
    .then((data)=>{
        console.log("done")
            
    })
    .catch(err => console.error(err))
},lastcallTime)

    lastcallTime += 3000

}

for(let i=0 ;i<6;i++){

callApi('https://jsonplaceholder.typicode.com/users')

}
setTimeout(()=>{
    lastcallTime=0
for(let i=0 ;i<6;i++){

    callApi('https://jsonplaceholder.typicode.com/users')
    
    }
},21000)


// 23:59:58
// done
// 00:00:01
// done
// 00:00:04
// done
// 00:00:07
// done
// 00:00:10
// done
// 00:00:13
// done
// 00:00:19
// done
// 00:00:22
// done
// 00:00:25
// done
// 00:00:28
// done
// 00:00:31
// done
// 00:00:34
// done


