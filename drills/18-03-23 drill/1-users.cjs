const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

// Q1 Find all users who are interested in playing video games.

function videoGamesUsers(){
   
    const userNames = Object.entries(users).reduce((acc,cur) =>{
        if(cur[1].interests[0].includes("Video Games")){
            acc.push(cur[0])
        }
        return acc;

    },[])
    return userNames
    
}
console.log(videoGamesUsers())

// Q2 Find all users staying in Germany.

function germanyUsers(){
    const userNames = Object.entries(users).reduce((acc,cur) =>{
        if(cur[1].nationality ==='Germany'){
            acc.push(cur[0])
        }
        return acc;

    },[])
    return userNames
}
console.log(germanyUsers())

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

function sortSeniority(){
    const makeLevels = Object.entries(users).sort((a,b) =>{
        const level = { "Intern" :1 , "Developer": 2, "Senior":3}
        const pattern = /(Developer|Intern|Senior)/g;

            if(level[a[1].desgination.match(pattern)[0]] > level[b[1].desgination.match(pattern)[0]]){
                return -1
            }else if(level[a[1].desgination.match(pattern)[0]] < level[b[1].desgination.match(pattern)[0]]){
                return 1
            }else{
                return b[1].age -a[1].age
            }
            
       

    })
    return makeLevels


}

console.log(sortSeniority())

// Q4 Find all users with masters Degree.

function masterUsers(){
    const userNames = Object.entries(users).reduce((acc,cur) =>{
        if(cur[1].qualification === 'Masters'){
            acc.push(cur[0])
            
        }
        return acc;

    },[])
    return userNames
}
console.log(masterUsers())


// Q5 Group users based on their Programming language mentioned in their designation.

function programmingUsers(){
    const getlang= Object.entries(users).reduce((acc,cur)=>{
    const wordsToRemove = ["Developer", "Intern", "-","Senior"];
    const pattern = new RegExp(wordsToRemove.join("|"), "gi");
    const result = cur[1].desgination.replace(pattern, "").trim();
    acc.push(result)
    return acc

    },[])
    const setlang =[...new Set(getlang)]
    const getlangUsers = setlang.reduce((acc,cur) =>{
        acc[cur] = Object.entries(users).reduce((acc1,cur1)=>{
            if(cur1[1].desgination.includes(cur)){
                acc1.push(cur1[0])
            }
            return acc1
        },[])
        return acc;
    },{})
    return getlangUsers

}

console.log(programmingUsers())