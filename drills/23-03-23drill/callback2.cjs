const lists = require('./lists.json')
// console.log(Object.entries(lists))

function callback2(bid, cb) {
    setTimeout(() => {
        if (arguments.length < 2 || bid === undefined || typeof bid != 'string' || typeof cb != 'function') {
            cb(new Error('error : Invalid board id.'))
        } else {

            if (bid in lists) {
                cb(null, lists[bid])
            } else {
                cb(new Error('error : Invalid board id.'))
            }


            
        }
    }, 2 * 1000);
}

// callback2("mcu453ed",function(err,data){
//     if(err){
//         console.log(err)
//     }else{
//         console.log(data)
//     }
// })

module.exports = callback2