const cards = require('./cards.json')
function callback3(listid, cb) {
    setTimeout(() => {
        if (arguments.length < 2 || listid === undefined || typeof listid != 'string' || typeof cb != 'function') {
            cb(new Error('error : Invalid list id.'))
        } else {

            if (listid in cards) {
                cb(null, cards[listid])
            } else {
                cb(new Error('error :data not found'))
            }



        }
    }, 2 * 1000);
}

// callback3("qwsa221",function(err,data){
//     if(err){
//         console.log(err)
//     }else{
//         console.log(data)
//     }
// })
module.exports = callback3