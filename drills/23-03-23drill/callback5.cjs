const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')

function callback5(bname, key, listname, cb) {
    setTimeout(() => {
        if (arguments.length < 3 || typeof bname != 'string' || typeof key != 'string' || typeof cb != 'function') {
            cb(new Error('error'))
        } else {

            callback1(bname, key, (err, data) => {
                if (err) {
                    cb(new Error('error'))
                } else {
                    cb(null, data)
                    callback2(data.id, (err, data2) => {
                        if (err) {
                            cb(new Error('error'))

                        } else {
                            cb(null, data2)
                            data2.forEach(subdata => {
                                if (listname.includes(subdata.name))  {
                                    callback3(subdata.id, (err, data3) => {
                                        if (err) {
                                            cb(new Error('error'))
                                        } else {
                                            cb(null, data3)
                                        }
                                    })
                                }
                            })
                        }

                    })
                }

            })


        }

    }, 2 * 1000);

}


// callback5('Thanos', 'name', ['Mind','Space'], (err, data) => {
//     if (err) {
//         console.log('erroe')

//     } else {
//         console.log(data)
//     }

// })
module.exports = callback5