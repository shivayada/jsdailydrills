const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')

function callback6(bname, key, cb) {
    setTimeout(() => {
        if (arguments.length < 3 || typeof bname != 'string' || typeof key != 'string' || typeof cb != 'function') {
            cb(new Error('error'))
        } else {

            callback1(bname, key, (err, data) => {
                if (err) {
                    cb(err)
                } else {
                    cb(null, data)
                    callback2(data.id, (err, data2) => {
                        if (err) {
                            cb(err)

                        } else {
                            cb(null, data2)
                            data2.forEach(subdata => {
                                callback3(subdata.id, (err, data3) => {
                                    if (err) {
                                        cb(err)
                                    } else {
                                        cb(null, data3)
                                    }
                                })
                            })
                        }

                    })
                }

            })


        }

    }, 2 * 1000);

}


// callback6('Thanos', 'name', (err, data) => {
//     if(err){
//         console.log(err)
//     }else{
//     console.log(data)
//     }
// })
module.exports = callback6