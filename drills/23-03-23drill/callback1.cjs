const boards = require('./boards.json')



function callback1(bid, key, cb) {
    setTimeout(() => {
        if (arguments.length < 2 || bid === undefined || typeof bid != 'string' || typeof cb != 'function') {
            cb(new Error('error : Invalid board id.'))
        } else {

            let data = boards.find(cur => {
                return cur[key] === bid
            })
            if (data === undefined) {
                cb(new Error('error: Did not found board id'))
            } else {
                cb(null, data)
            }



        }
    }, 2 * 1000);
}

// callback1("mcu453ed",'id',function(err,data){
//     if(err){
//         console.log(err)
//     }else{
//         console.log(data)
//     }
// })

module.exports = callback1